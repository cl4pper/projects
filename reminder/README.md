# reminder

> My own notes and todo list app with firebase realtime database.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).


TECHS(to install using npm):

Bootstrap-vue: bootstrap-vue

Fontawesome: https://fontawesome.com/how-to-use/on-the-web/using-with/vuejs

Sass: sass-loader node-sass style-loader
