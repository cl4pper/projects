# templates-components-vue

> A project repo for holding components templates and re-uese in future projects.

1. BOOTSTRAP: bootstrap-vue
2. SASS packages: sass-loader | node-sass | style-loader
3. VUETIFY: vuetify | material-design-icons-iconfont


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
