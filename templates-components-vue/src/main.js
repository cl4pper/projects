import Vue from 'vue'
import App from './App.vue'
// TO USE BOOTSTRAP
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// TO USE VUETIFY
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(BootstrapVue);
Vue.use(Vuetify);

new Vue({
  el: '#app',
  render: h => h(App)
})
